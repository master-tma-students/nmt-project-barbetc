#Created by clement on 21/10/2022 
#Used to understand basic mecanics of fluidsim use.
#
#Follows closely the page : https://fluidsim.readthedocs.io/en/latest/ipynb/tuto_user.html

#You need to create and activate env_fluidsim in mamba/conda before using this script.

from __future__ import print_function
import fluidsim 
import matplotlib.pyplot as plt
from fluidsim import load_sim_for_plot
from fluidsim.solvers.ns2d.solver import Simul  # You can also use Simul = fluidsim.import_simul_class_from_key("ns2d")
from fluidsim import load_state_phys_file
import shutil
import time
from pathlib import Path

params = Simul.create_default_params() #Sets up parameters used to define the sim. Using default values;

#### We can change the parameters if we want to. 

params.nu_2 = 1e-6
#params.forcing.enable = False

params.init_fields.type = "noise"

params.output.periods_save.spatial_means = 1.0 
params.output.periods_save.spectra = 1.0
params.output.periods_save.phys_fields = 2.0

### to print list of all parameters : 
#print([attr for attr in dir(params.output) if not attr.startswith("_")])

sim = Simul(params)         #initiates simulation.
sim.time_stepping.start()   #Launching simulation.

####comment to avoid this appearing in your terminal
#print(load_sim_for_plot.__doc__)
#print(load_state_phys_file.__doc__)

#loading output files for plotting.
sim = load_state_phys_file(sim.output.path_run)

####plotting some of our results.
sim.output.spatial_means.plot()

#plotting the final state.
sim.output.phys_fields.plot()

#plotting at a different time step.
sim.output.phys_fields.plot(time=4)
fig = plt.gcf()

plt.show()

### saving the figures in a directory.
#sim.output.print_stdout() 
## I don't manage to use these properly yet.
#sim.output.phys_fields.save(state_phys=None, params=None, particular_attr=None)

shutil.rmtree(sim.output.path_run) #Cleaning the results from memory.


#La bonne librairie pour gérer chemins : pathlib. "from pathlib import Path"
