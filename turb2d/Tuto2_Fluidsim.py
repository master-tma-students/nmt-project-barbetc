#Created by clement on 22/10/2022 
#Used to understand basic mecanics of fluidsim use.
#
#Follows closely the page : https://fluidsim.readthedocs.io/en/latest/ipynb/tuto_dev.html

#You need to create and activate env_fluidsim in mamba/conda before using this script.

#importing some useful libraries from Tuto 1

from __future__ import print_function
import fluidsim 
import matplotlib.pyplot as plt
from fluidsim import load_sim_for_plot
from fluidsim.solvers.ns2d.solver import Simul  # You can also use Simul = fluidsim.import_simul_class_from_key("ns2d")
from fluidsim import load_state_phys_file
import shutil
import time

