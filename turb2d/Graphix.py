"""
This program was written by Clement Barbet on 31/12/2022.

Its purpose is to render, display and/or save results obtained from 2d Navier-Stokes resolution 
using fluidsim software. It requires mainly the fluidsim library installed/included in your current python
interpreter to run, between others. 
"""

from __future__ import print_function
from pathlib import Path
import fluidsim 
import matplotlib.pyplot as plt
from fluidsim import load_sim_for_plot
#from fluidsim.solvers.ns2d.solver import Simul  
# You can also use Simul = fluidsim.import_simul_class_from_key("ns2d")
from fluidsim import load_state_phys_file
import os
import netCDF4
import h5py as h
import shutil
import time

path = Path('/home/clement/Sim_data/NS2D_96x96_S8x8_2022-12-31_11-04-44')
path.exists()
os.chdir(path)

file = h.File('spectra2D.h5', 'r')
print(file.keys())



