# SIMULATIONS FOR 2D TURBULENCE REALISED WITH FLUIDSIM.

## List of contents : 


- Tuto1.py
- Tuto2.py
- Exo_Spectres_Stationnaires.py
- Turb2d_images containing 4 directories :

	- 256x256_L=2_t60 : Contains the result graphs of our simulations of enstrophy and energy spectrums for a grid of nh = nx = ny = 256, L=2 and t=60 as parameters of the simulation. These results are obtained with Exo_spectres_stationnaires script, where we used forcing 'tcrandom'. 

	- 256x256_L=2pi_t60 : Contains the result graphs of our simulations of enstrophy and energy spectrums for a grid of nh = nx = ny = 256, L=2pi and t=60 as parameters of the simulation. These results are obtained with Exo_spectres_stationnaires script, where we used forcing 'tcrandom'. 

	- 256x256_L=2_t60 : Contains the result graphs of our simulations of enstrophy and energy spectrums for a grid of nh = nx = ny = 256, L=9 and t=40 as parameters of the simulation. These results are obtained with Exo_spectres_stationnaires script, where we used forcing 'tcrandom'.

	- Test256x256: Contains the result graphs of our test simulations of enstrophy and energy spectrums for a grid of nh = nx = ny = 256. 
	
