#Created by Clem on 28/10.
#
#Help can be found here https://fluidsim.readthedocs.io/en/latest/ipynb/tuto_dev.html
#
#You need to create and activate env_fluidsim in mamba/conda before using this script.
#
#importing some useful libraries from Tuto 1

from __future__ import print_function
from pathlib import Path
#import fluidsim 
import matplotlib.pyplot as plt
#from fluidsim import load_sim_for_plot
from fluidsim.solvers.ns2d.solver import Simul  
# You can also use Simul = fluidsim.import_simul_class_from_key("ns2d")
from fluidsim import load_state_phys_file
#import shutil
from math import pi

#-----------------------------------EXERCISE---------------------------------------------------------
## OUR AIM IS TO PLOT AND STORE THE SPECTRAS OF 
## DIFFERENT FORCED FLOWS (Meaning they are statisticaly stationnary)
#----------------------------------------------------------------------------------------------------


#PARAMS----------------------------------------------------------------------------------------------
params = Simul.create_default_params() 
#Sets up parameters used to define the sim. Using default values

params.nu_2 = 1e-6   #-------------------------We take the value in water for viscosity.
params.forcing.enable = True #-----------------Enables forcing
params.forcing.type = "tcrandom" #---------------Is right. 
params.init_fields.type = "noise"

params.oper.nx = nh = 256
params.oper.ny = nh
params.oper.Lx = Lh = 2*pi
params.oper.Ly = Lh

params.time_stepping.t_end = 100.0
params.output.periods_save.spatial_means = 1.0 
params.output.periods_save.spectra = 0.5
params.output.periods_save.spect_energy_budg = 0.5
params.output.periods_save.temporal_spectra = 0.5
params.output.periods_save.phys_fields = 1.0

#SIMULATION------------------------------------------------------------------------------------------
#We want to observe conservation of Energy and Enstrophy in spectres.

sim = Simul(params)         #On instancie la classe Simul à l'aide de params.
sim.time_stepping.start()   #Launching simulation.

#loading output files for plotting.
sim = load_state_phys_file(sim.output.path_run)

sim.output.spectra.plot1d()
####plotting some of our results.
sim.output.spectra.plot2d()

#plotting the final state.
sim.output.temporal_spectra.plot_spectra()

#plotting at a different time step.
sim.output.spect_energy_budg.plot()
plt.show()
