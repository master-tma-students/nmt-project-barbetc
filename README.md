# nmt-project-barbetc

## Projet Numerical Methods for Turbulence - Clément Barbet
Projet pédagogique dans le cadre de l'UE "Méthodes numériques pour la turbulence" au sein du Master 2 "Turbulence Méthodes et applications". 

## Contenu (03/02/2023) : 
- 1 dossier turb2d contenant :
    - Exo_Spectres_Stationnaires.py : prog python nécéssitant fluidsim
    - README_turb2d.md 
    - Tuto1_fluidsmi.py : prog python réalisé à titre d'entrainement.
    - Tuto2_Fluidsim.py : prog python réalisé à titre d'entrainement. 
    - Graphix.py : prog python destiné à la lecture/affichage graphique de résultats obtenus par les autres programmes.
- 1 dossier d'images de turbulence 2d, contenant résultats de simulation, voir readme.md de turb2d.
- 1 dossier turb3d contenant : 
    - Images de résultats de turbulence 3d. 
    - 1 jupyter V3 résumant les activtés effectuées et les résultats obtenus pour les exercices 1 à 10. 
    - 1 dossier "practical_chapter2" contenant des résultats sur tenibre.vnc.

## Contenu ajouté 31/12/2022:
- 1 dossier pour simulations en turbulence 3D et dépos de notre avancée à ce stade dans ces contenus et graphiques utiles. 
- Upload d'images dans turb2d. 

## Contenu ajouté 28/01/23:
- Images de turb2d (commit ajout images)
- Changements dans les readme
- updates de scripts turb2d

## Contenu ajouté 03/02/23:
- Notebook .ipynb contenant les réponses pour les exercices 1 à 10 de turb3d, donné par Mr. Balarac
- Modifications readme.md
- Ajout du notebook travaillé avec Mr. Picard. 
- Nettoyage de certains fichiers devenus caduques. 

### Problèmes rencontrés : 
Lors de certains commit, une erreur 500 était affichée lisant après hg push : 

abandon : git remote error: unexpected http resp 500 for https://gricad-gitlab.univ-grenoble-alpes.fr/master-tma-students/nmt-project-barbetc.git/git-receive-pack

Elle fut résolue après avoir reclone depuis GitLab et push des résultats déjà sauvegardés localement ailleurs que dans le repo local.
 



